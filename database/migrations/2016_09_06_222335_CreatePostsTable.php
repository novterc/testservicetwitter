<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigInteger('post_id')->primary();
            $table->bigInteger('user_id')->index();    
            $table->dateTime('datetime');
            $table->string('title',255);
            $table->mediumText('description');
            $table->integer('num_favorites');
            $table->integer('num_replies');
            $table->integer('num_retweets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
