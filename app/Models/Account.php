<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Abraham\TwitterOAuth\TwitterOAuth;

class Account extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'refresh_interval', 'title', 'posts_number',
    ];
    protected $primaryKey = 'user_id';

    public function scopeBasicFields($query) {
        return $query->select('user_id as account_id', 'title', 'refresh_interval', 'posts_number');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post','user_id');
    }

    public function loadPosts() {
        $connect = new TwitterOAuth(
            env('TWITTER_CONSUMER_KEY'),
            env('TWITTER_CONSUMER_SECRET'),
            env('TWITTER_ACCESS_TOKEN'),
            env('TWITTER_ACCESS_TOKEN_SECRET')
        );

        $result = $connect->get("statuses/user_timeline", ["user_id" => $this->user_id, 'count' => 200]);
        if(isset($result->errors)){
            throw new \Exception(json_encode($result->errors));
        }

        foreach ($result as $post) {
            $postObj = Post::firstOrNew([
                'post_id' => $post->id,
                'user_id' => $post->user->id,
                'datetime' => strtotime( $post->created_at ),
                'description' => $post->text,
            ]);
            
            $postObj->num_favorites = $post->favorite_count;
            $postObj->num_replies = 0;
            $postObj->num_retweets = $post->retweet_count;
            $postObj->save();
        }
    }
}
