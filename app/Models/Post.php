<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'user_id', 'datetime', 'title',  'description', 'num_favorites', 'num_replies', 'num_retweets',
    ];
    protected $primaryKey = 'post_id';

    public function scopeBasicFields($query) {
        return $query->select('post_id', 'title', 'datetime', 'description', 'num_favorites', 'num_replies', 'num_retweets');
    }

    public function offers()
    {
        return $this->belongsTo('App\Models\Account', 'user_id');
    }

}
