<?php 

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Console\Commands\TestCommand;

class CommandServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.test.command', function()
        {
            return new TestCommand;
        });

        $this->commands(
            'command.test.command'
        );
    }
}