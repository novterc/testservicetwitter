<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Post;

class ApiController extends BaseController
{
	protected static $_rulesValidation = [
		'account_id' => 'required|integer|min:1|unique:accounts,user_id',
	    'refresh_interval' => 'required|integer|min:1'
	];
    
    public function accountNew(Request $request) {
    	$rules = static::$_rulesValidation;
	    $validator = \Validator::make( $request->all(), $rules);
        if( $validator->fails() ){
        	$messages = $validator->messages()->all();
        	return $this->_getErrorResponse($messages);
        }

    	$user_id = $request->input('account_id');
    	$refresh_interval = $request->input('refresh_interval');

    	$result = $this->_getConnection()->get("users/show", ["user_id" => $user_id]);
    	if(isset($result->errors)){
    		$messages = array_map(function($item){ return $item->message; }, $result->errors);
    		return $this->_getErrorResponse($messages);
    	}

    	$account = Account::create([
    		'user_id' => $user_id,
    		'refresh_interval' => $refresh_interval,
    		'title' => $result->screen_name,
    	]);

    	return $this->_getSuccessResponse([
			'title' => $result->screen_name,
    	]);
    }

    public function accounts() {
    	$accounts = Account::basicFields()->get();
    	return response()->json($accounts->toArray());
    }

    public function accountEdit(Request $request, $accountId) {
    	$rules = [
    		'refresh_interval' => static::$_rulesValidation['refresh_interval'],
    	];
	    $validator = \Validator::make( $request->all(), $rules);
        if( $validator->fails() ){
        	$messages = $validator->messages()->all();
        	return $this->_getErrorResponse($messages);
        }
    	$refresh_interval = $request->input('refresh_interval');

    	$account = Account::find($accountId);
    	if($account===null){
    		return $this->_getErrorResponse('not found account');
    	}

    	$account->refresh_interval = $refresh_interval;
    	$account->save();

    	return $this->_getSuccessResponse([
			'title' => $account->title,
    	]);
    }

    public function accountDelete($accountId) {
    	$account = Account::find($accountId);
    	if($account===null){
    		return $this->_getErrorResponse('not found account');
    	}

    	$account->posts()->delete();
    	$account->delete();

    	return $this->_getSuccessResponse();
    }

    public function posts(Request $request, $accountId) {
    	$account = Account::find($accountId);
    	if($account===null){
    		return $this->_getErrorResponse('not found account');
    	}

    	$limit = $request->input('limit', 100);
    	$posts = Post::basicFields()->limit($limit)->get();

    	return response()->json($posts->toArray());
    }

    protected function _getConnection() {
    	return new TwitterOAuth(
    		env('TWITTER_CONSUMER_KEY'), 
    		env('TWITTER_CONSUMER_SECRET'), 
    		env('TWITTER_ACCESS_TOKEN'), 
    		env('TWITTER_ACCESS_TOKEN_SECRET')
    	);
    }

    protected function _getErrorResponse($description) {
    	if( is_array($description) )
    		$description = implode(',', $description);
    	return response()->json([
    		'status' => 'error', 
    		'description' => $description,
    	]);
    }

    protected function _getSuccessResponse($data = []) {
    	$data['status'] = 'success';
    	return response()->json($data);
    }

}
