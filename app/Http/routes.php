<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


	use Abraham\TwitterOAuth\TwitterOAuth;

$app->get('/', function() use($app) {
	return time();	
});

$app->group(['prefix' => 'accounts'], function ($app) {
	$app->post('new', 'App\Http\Controllers\ApiController@accountNew' );
	$app->get('/', 'App\Http\Controllers\ApiController@accounts' );
	$app->post('{accountId}', 'App\Http\Controllers\ApiController@accountEdit' );
	$app->post('{accountId}/delete', 'App\Http\Controllers\ApiController@accountDelete' );
	$app->get('{accountId}/posts', 'App\Http\Controllers\ApiController@posts' );
});