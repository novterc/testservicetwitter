<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Models\Post;

class TestCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run test command.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $connect = new TwitterOAuth(
            env('TWITTER_CONSUMER_KEY'), 
            env('TWITTER_CONSUMER_SECRET'), 
            env('TWITTER_ACCESS_TOKEN'), 
            env('TWITTER_ACCESS_TOKEN_SECRET')
        );

        $result = $connect->get("statuses/user_timeline", ["user_id" => 768770739491266562, 'count' => 20]);
        if(isset($result->errors)){
            throw new \Exception(json_encode($result->errors));
        }

        foreach ($result as $post) { 
            var_dump( $post, '---------------------------------------------------------------------' ); 
            $postObj = Post::firstOrNew([
                'post_id' => $post->id,
                'user_id' => $post->user->id,
                'datetime' => date('Y-m-d H:i:s', strtotime( $post->created_at )),
                'description' => $post->text,                
            ]);

            $postObj->num_favorites = $post->favorite_count;
            $postObj->num_replies = 0;
            $postObj->num_retweets = $post->retweet_count;
            $postObj->save();
        }
    }

}
